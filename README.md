# Listofplanets

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.0.

## Installation Guide
First of all you need to clone this repository. Please use `git clone https://kochanone@bitbucket.org/kochanone/listofplanetslk.git`.
Project files will be cloned to selected location. 
Then use `npm install` to auto-download all dependecies.
Then you can finally use `ng serve` to run the server. Use address `http://localhost:4200/` in your web browser to visit the site.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Author
Lukasz Kochanowski
luke.kochanowski@gmail.com

# Possible updates
This project can be updated, especially list view and detail view : 

- list : add selector to view table istead of cards

- list : add singleton service to hold results in memory while checking up this service

- list : singleton service : check on timer subscription and get new fresh data from API

- detail : when you click "films" or "residents" link you are being transfered to outer site. There could ba an additional message window to ask if you are sure that you will be redirected (PrimeNG - ConfirmationService).