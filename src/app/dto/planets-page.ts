import {Planet} from './planet';

export class PlanetsPage{
  count : number;
  next: string;
  previous: string;
  results:  Planet[];
}
