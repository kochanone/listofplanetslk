export class Planet{

  name: string;
  rotation_period: number;
  orbital_period: number;
  diameter: 12500;
  climate: string;
  gravity: string;
  terrain: string;
  surface_water: number;
  population: number;
  residents: string[];
  films: string[];
  created: Date;
  edited: Date;
  url: String;

}
