import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {PlanetsPage} from './dto/planets-page';
import {Planet} from './dto/planet';
import {reject, resolve} from 'q';

@Injectable()
export class PlanetService {

  private api = 'https://swapi.co/api/planets/';

  constructor(private http: HttpClient) {
  }

  getPlanets(page: number): Observable<PlanetsPage> {
    let pagerString = '';
    if (page == 1) {
      pagerString = '';
    } else {
      pagerString = '?page=' + page.toString();
    }
    return this.http.get<PlanetsPage>(this.api + pagerString);
  }

  getPlanetDetails(planetId: number): Observable<Planet> {
    return this.http.get<Planet>(this.api + planetId);
  }

}
