import {Component, OnInit} from '@angular/core';
import {Planet} from '../dto/planet';
import {PlanetService} from '../planet.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-planet-details',
  templateUrl: './planet-details.component.html',
  styleUrls: ['./planet-details.component.css']
})
export class PlanetDetailsComponent implements OnInit {

  constructor(private service: PlanetService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  planetId : number;
  planet: Planet = new Planet;

  load : boolean = false;

  ngOnInit() {
    this.planet.films = [];
    this.planet.residents = [];

    this.load = true;
    this.route.params.subscribe(params => {
      this.planetId = params['id'];
      this.service.getPlanetDetails(this.planetId).subscribe(planet =>{
        this.planet = planet;
        this.load = false;
      });
    });
  }

  //Getting back to list '/list'
  back(){
    this.location.back();
  }

}
