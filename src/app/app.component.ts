import { Component } from '@angular/core';
import {PlanetService} from './planet.service';
import {PlanetsPage} from './dto/planets-page';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() {
  }

}
