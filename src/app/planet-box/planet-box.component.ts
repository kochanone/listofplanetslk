import {Component, Input, OnInit} from '@angular/core';
import {Planet} from '../dto/planet';
import {Router} from '@angular/router';

@Component({
  selector: 'app-planet-box',
  templateUrl: './planet-box.component.html',
  styleUrls: ['./planet-box.component.css']
})
export class PlanetBoxComponent implements OnInit {

  constructor(private router: Router) {
  }

  @Input() planet: Planet;

  ngOnInit() {
  }

  goDetails() {
    this.router.navigate(['planet/' + this.getPlanetId()]);
  }

  //Need to navigate to planet using route 'planet/:id'
  getPlanetId(): string {
    let sliced = this.planet.url.split('/');
    console.log(sliced);
    if (sliced[sliced.length-1]){
      return sliced[sliced.length-1];
    } else {
      return sliced[sliced.length-2];
    }
  }

}
