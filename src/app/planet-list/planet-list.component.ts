import {Component, OnInit} from '@angular/core';
import {PlanetService} from '../planet.service';
import {Planet} from '../dto/planet';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-planet-list',
  templateUrl: './planet-list.component.html',
  styleUrls: ['./planet-list.component.css']
})
export class PlanetListComponent implements OnInit {

  constructor(private planetService: PlanetService) {
    this.pageOptions = [
      {label: '5', value: 5},
      {label: '10', value: 10},
      {label: '25', value: 25},
      {label: '100', value: 100}
    ];
  }

  pageOptions: SelectItem[];

  planetsAll: Planet[] = [];

  filter: string = "";

  shownPlanet: Planet[] = [];

  selectedOption: number = 25;

  nextPage: boolean = false;
  previousPage: boolean = false;

  pageShown: number = 0;

  load: boolean = false;

  ngOnInit() {
    this.getAllPlanets(1);
    if (this.shownPlanet.length < this.planetsAll.length) {
      this.nextPage = true;
    } else {
      this.nextPage = false;
    }
  }

  //Method getting all planets using recursion. It is needed to paginate results with our rules so we have to get all records, due to limited usage of API, and it's own pagination.
  getAllPlanets(i: number) {
    this.load = true;
    this.planetService.getPlanets(i).subscribe(res => {
      this.planetsAll = this.planetsAll.concat(res.results);
      if (res.next) {
        this.getAllPlanets(i + 1);
      } else {
        //this.shownPlanet = Object.assign({}, this.planetsAll);
        this.shownPlanet = this.planetsAll.map(x => Object.assign({}, x));
        this.filterList();
        this.load = false;
      }
    })
  }

  //Method filtering the list in case user uses input.
  //Used to change sites.
  filterList() {
    this.shownPlanet = this.sliceList(this.pageShown);

    if (this.checkiflast()) {
      this.nextPage = false;
    } else {
      this.nextPage = true;
    }
  }

  //Part of filtering method. This slices list of planets on pages.
  sliceList(page): Planet[] {
    if (this.filter) {
      return this.planetsAll.filter(planet => planet.name.toLowerCase().includes(this.filter.toLowerCase())).slice(page * this.selectedOption, ((page + 1) * this.selectedOption));
    } else {
      return this.planetsAll.slice(page * this.selectedOption, ((page + 1) * this.selectedOption));
    }
  }

  //Next page click method.
  nextPageClick() {
    this.pageShown += 1;
    this.filterList();
  }

  //Previous page click method.
  prevPageClick() {
    this.pageShown -= 1;
    if (this.pageShown == 0) {
      this.previousPage = false;
    }
    this.filterList();
  }

  //Method checking if there is more records than shown.
  //Important when using filter. Detects if next page button is able to appear.
  checkiflast(): boolean {
    if (this.shownPlanet.length < this.selectedOption) {
      return true;
    }
    try {
      if (this.sliceList(this.pageShown + 1).length == 0) {
        return true;
      }
    } catch (e) {
      return true;
    }
  }

}
