import {AppComponent} from './app.component';
import {PlanetService} from './planet.service';
import {PlanetBoxComponent} from './planet-box/planet-box.component';
import {PlanetListComponent} from './planet-list/planet-list.component';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {ProgressSpinnerModule, SelectButtonModule} from 'primeng/primeng';
import {RouterModule, Routes} from '@angular/router';
import {PlanetDetailsComponent} from './planet-details/planet-details.component';
import {LoaderComponent} from './loader/loader.component';

const appRoutes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: PlanetListComponent},
  {path: 'planet/:id', component: PlanetDetailsComponent},
  {path: '**', component: PlanetListComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    PlanetBoxComponent,
    PlanetListComponent,
    PlanetDetailsComponent,
    LoaderComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false}
    ),
    BrowserModule,
    HttpClientModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    SelectButtonModule,
    ProgressSpinnerModule
  ],
  providers: [PlanetService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
